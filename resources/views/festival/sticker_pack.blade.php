@extends('layouts.master')
@section('content')
 <div class="span9">
        <div class="content">
            <div class="btn-controls">
 <div class="span9" style="float: right;">
                    <div class="content">
                        <div class="module"> 
                        <div class="module-head">
                                <h3>
                                   {{ $flag }} APP</h3>
                            </div>                          
                            <div class="module-option clearfix">                                
                                <div class="btn-group pull-right" data-toggle="buttons-radio">
                                         Delete all record just check on  <input type="checkbox" id="master" style="width: 25px;height: 25px;margin-left: 10px;border-right-width: 10px;margin-right: 13px;">   <button style="background: red" class="btn btn-primary delete_all" data-url="{{ url('myproductsDeleteAll') }}">Delete </button> 
                                </div>
                              <div class="btn-group pull-left" data-toggle="buttons-radio"> 
                                <a href="{{route('festival.create',$flag)}}" style="color: white">
                                    <button type="button" class="btn">
                                        Add Pack  {{ $flag }}</button></a>
                                    <a href="{{route('festival.create.multiimage',['id'=>'festival-list','flag'=>$flag])}}" style="color: white">
                                    <button type="button" class="btn">
                                       Multiple Add  {{ $flag }}</button></a>
                                    </div>
                            </div>                           
                        </div>
                    </div>
                    <!--/.content-->
                </div>
                  </h4> 

                  @if (count($stickerdetail) > 0 )      
                                                         
                  @foreach($stickerdetail as $key=>$sticker)
                            @if (count($sticker['festival_sticker']) > 0)
                            <div> </div>
                             &nbsp;&nbsp; 
                        <div class="module-head">
                               <h3>Pack Name :  {{ $sticker['pack_name'] }}</h3>
                         </div>
                           <div class="btn-box-row row-fluid"> 
                                @foreach($sticker['festival_sticker'] as $value)
                                  <a href="#" class="btn-box big span4" style="margin-left: 0px; margin-right: 10px;"> 
                                  <img src="{{ $value['sticker'] }}" style="width: 150px;height: 100px;">
                               <p class="text-muted">
                                <b style="font-size:17px;">
                                {{ strlen($value['name']) > 20 ? substr($value['name'],0,20)."..." : $value['name']  }}

                             </b>
                                 <input type="checkbox"  class="sub_chk" data-id="{{$value['id']}}" style="width: 15px;ight: 15px;">
                                 <img src="{{ url('icon/delete-24px.svg') }}"  style="cursor: pointer;" onclick='deletesticker("{{$value["id"]}}")'></p>
                               </a>
                                @endforeach 
                                 </div>
                              @else
                                    <div> </div>
                                    &nbsp;&nbsp; 
                                    <div class="module-head">
                                           <h3>Pack Name :  {{ $sticker['pack_name'] }}</h3>
                                     </div>
                                      <div class="btn-box-row row-fluid" style="text-align: center;"> 
                                        <b>No Sticker Found.</b>                               
                                 </div>
                          @endif
                  @endforeach
               
          @endif                                                            
            </div>                          
        </div>
</div>


<script src="{{ asset('mobile_app_css/js/core/jquery.min.js') }}"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

         <script src="{{ asset('scripts/jquery-1.9.1.min.js') }}" type="text/javascript"></script>
<script type="text/javascript"> 
  
  function deletesticker(id) { 
      swal({
              title: "Are you sure?",
              text: "You will not be able to recover this data file!",
              type: "error",
              showCancelButton: true,
              dangerMode: true,
              cancelButtonClass: '#DD6B55',
              confirmButtonColor: '#dc3545',
              confirmButtonText: 'Delete!',
          }).then(function(isconform) { 
          if (isconform.value) {
            $.ajax({
                       url: "{{route('sticker.delete')}}",
                       type: 'post',
                        data: {
                          '_token':'{{ csrf_token() }}',                         
                          'id': id,                          
                      }, success: function(data) {
                           location.reload(true);
                          },
                  });
          }    
          });
              } 
  </script> 
 <script src="{{ asset('scripts/jquery-1.9.1.min.js') }}" type="text/javascript"></script>
   <script type="text/javascript">
    $(document).ready(function () {       
        $('#master').on('click', function(e) {
         if($(this).is(':checked',true))  
         {
            $(".sub_chk").prop('checked', true);  
         } else {  
            $(".sub_chk").prop('checked',false);  
         }  
        }); 


                  $('.delete_all').on('click', function (event) {
                      event.preventDefault();                      
                       var allVals = [];  
                        $(".sub_chk:checked").each(function() {  
                            allVals.push($(this).attr('data-id'));
                        });

                       if(allVals.length <= 0)  
                          {                              
                          swal({      
                          title: "Please select sticker.",                          
                          type: "error",                                          
                      });                         
                          }  else {
                          swal({
                          title: "Are you sure?",
                          text: "You will not be able to recover this data file!",
                          type: "error",
                          showCancelButton: true,
                          dangerMode: true,
                          cancelButtonClass: '#DD6B55',
                          confirmButtonColor: '#dc3545',
                          confirmButtonText: 'Delete!',
                      }).then(function(isconform) {
                        if (isconform.value) {                         
                            var join_selected_values = allVals.join(",");                   
                              $.ajax({
                                   url: "{{route('sticker.multidelete')}}",
                                   type: 'DELETE',
                                   data: {
                                      '_token':'{{ csrf_token() }}',
                                      'id': join_selected_values,                                                              
                                     }, success: function(data) {
                                       location.reload(true);
                                      },
                              });
                            $.each(allVals, function( index, value ) {
                                $('table tr').filter("[data-row-id='" + value + "']").remove();
                            });        
                          }
                      });
                          }                      
                  }); 
      });
</script> 
@endsection

