
@extends('layouts.master')
@section('content')
<div class="span9">
          <div class="content">

            <div class="module">
              <div class="module-head">
                <h3>Forms</h3>
              </div>
              <div class="module-body">
                  <br>
                  <form class="form-horizontal row-fluid" method="POST" action="{{route('festival.store')}}" enctype="multipart/form-data">
                    @csrf
                   <input type="hidden" name="flag" value="{{ $flag }}">
                     @if ($flag == "sticker") 
                    <div class="control-group">
                      <label class="control-label" for="basicinput">Name</label>
                      <div class="controls">
                        <select class="span8" name="pack_id">   
                                  <option value="">Select Sticker Pack</option>    
                                  @foreach ($packdetail as $key => $value)
                                     <option value="{{ $value->id }}"> 
                                              {{ $value->name }} 
                                      </option>
                                  @endforeach    
                           </select>
                            @if($errors->has('pack_id'))
                           <span class="error">
                            {{ $errors->first('pack_id') }}
                                  </span>
                          @endif
                      </div>
                    </div>
                        @endif

                    <div class="control-group">
                      <label class="control-label" for="basicinput">Name</label>
                      <div class="controls">
                         <input type="text" id="basicinput"  name="name" placeholder="Type something here..." class="span8">
                          @if($errors->has('name'))
                           <span class="error">
                            {{ $errors->first('name') }}
                                  </span>
                          @endif
                      </div>
                    </div>

                  @if ($flag == "sticker") 
                    <div class="control-group">
                            <label class="control-label" for="basicinput">Files</label>
                            <div class="controls">
                                <input type="file" id="basicinput" accept="image/webp" placeholder="Type something here..." class="span8" name="image">
                                @if($errors->has('image'))
                                   <span class="error">
                                    {{ $errors->first('image') }}
                                          </span>
                                @endif
                           </div>
                    </div>
                    @endif
                     @if ($flag == "photo") 
                    <div class="control-group">
                            <label class="control-label" for="basicinput">Files</label>
                            <div class="controls">
                                <input type="file" id="basicinput" accept="image/png,image/jpg, image/jpeg" placeholder="Type something here..." class="span8" name="image">
                                @if($errors->has('image'))
                                   <span class="error">
                                    {{ $errors->first('image') }}
                                          </span>
                                @endif
                           </div>
                    </div>
                    @endif

                     @if ($flag == "video") 
                    <div class="control-group">
                            <label class="control-label" for="basicinput">Files</label>
                            <div class="controls">
                                <input type="file" id="file-to-upload" accept="video/mp4,video/x-m4v,video/*" placeholder="Type something here..." class="span8" name="video">
                                @if($errors->has('image'))
                                   <span class="error">
                                    {{ $errors->first('image') }}
                                          </span>
                                @endif
                           </div>
          <div class="control-group">
             <div class="controls">
                            <video id="main-video" controls style="width: 250px; height: 125px; display: none;">
                      <source type="video/mp4">
                        </video>
                              <canvas id="video-canvas" style=" display: none;"></canvas>
                                    <div id="set-video-seconds">
                        <input type="hidden" name="thumbnail" id="put-thumbnail">
                    </div>
                  </div>
                    @endif
                  </div>

                    <div class="control-group">
                      <div class="controls">
                        <button type="submit" class="btn">Submit Form</button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>

            
            
          </div><!--/.content-->
        </div>

        <script>
  function formsubmit(){
    document.getElementById("loading").style.display = "block";
  }
var _CANVAS = document.querySelector("#video-canvas"),
  _CTX = _CANVAS.getContext("2d"),
  _VIDEO = document.querySelector("#main-video");

document.querySelector("#file-to-upload").addEventListener('change', function() {
   
  document.querySelector("#main-video source").setAttribute('src', URL.createObjectURL(document.querySelector("#file-to-upload").files[0]));
 
  _VIDEO.load();
  _VIDEO.style.display = 'inline'; 
 
});

document.querySelector("#main-video").addEventListener('timeupdate', function() {
  _VIDEO.currentTime = 5;
   _CTX.drawImage(_VIDEO, 0, 0, 300, 150);
  document.querySelector("#put-thumbnail").setAttribute('value', _CANVAS.toDataURL());   
});
</script>
         @endsection
