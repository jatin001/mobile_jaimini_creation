
@extends('layouts.master')
@section('content')
<div class="span9">
          <div class="content">

            <div class="module">
              <div class="module-head">
                <h3>Forms</h3>
              </div>
              <div class="module-body">
                  <br>
                  <form class="form-horizontal row-fluid" method="POST" action="{{route('festival.store.savemultiimage','multiimage')}}" enctype="multipart/form-data">
                    @csrf
                   <input type="hidden" name="flag" value="{{ $flag }}">
                     @if ($flag == "sticker") 
                    <div class="control-group">
                      <label class="control-label" for="basicinput">Name</label>
                      <div class="controls">
                        <select class="span8" name="pack_id">   
                                  <option value="">Select Sticker Pack</option>    
                                  @foreach ($packdetail as $key => $value)
                                     <option value="{{ $value->id }}"> 
                                              {{ $value->name }} 
                                      </option>
                                  @endforeach    
                           </select>
                            @if($errors->has('pack_id'))
                           <span class="error">
                            {{ $errors->first('pack_id') }}
                                  </span>
                          @endif
                      </div>
                    </div>
                        @endif                   

                  @if ($flag == "sticker") 
                    <div class="control-group">
                            <label class="control-label" for="basicinput">Files</label>
                            <div class="controls">
                                <input type="file" id="basicinput" accept="image/webp" placeholder="Type something here..." class="span8" name="image[]" multiple>
                                @if($errors->has('image'))
                                   <span class="error">
                                    {{ $errors->first('image') }}
                                          </span>
                                @endif
                           </div>
                    </div>
                    @endif
                     @if ($flag == "photo") 
                    <div class="control-group">
                            <label class="control-label" for="basicinput">Files</label>
                            <div class="controls">
                                <input type="file" id="basicinput" accept="image/png,image/jpg, image/jpeg" placeholder="Type something here..." class="span8" name="image[]" multiple>
                                @if($errors->has('image'))
                                   <span class="error">
                                    {{ $errors->first('image') }}
                                          </span>
                                @endif
                           </div>
                    </div>
                    @endif                  

                    <div class="control-group">
                      <div class="controls">
                        <button type="submit" class="btn">Submit Form</button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>

            
            
          </div><!--/.content-->
        </div>
         @endsection
