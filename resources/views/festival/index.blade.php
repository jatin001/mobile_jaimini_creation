

@extends('layouts.master')
@section('content')
 <div class="span9">
        <div class="content">
            <div class="btn-controls">
 <div class="span9" style="float: right;">
                    <div class="content">
                        <div class="module"> 
                        <div class="module-head">
                                <h3>
                                   {{ $flag }} APP</h3>
                            </div>                          
                            <div class="module-option clearfix">                                
                                <div class="btn-group pull-right" data-toggle="buttons-radio">
                                         Delete all record just check on  <input type="checkbox" id="master" style="width: 25px;height: 25px;margin-left: 10px;border-right-width: 10px;margin-right: 13px;">   <button style="background: red" class="btn btn-primary delete_all" data-url="{{ url('myproductsDeleteAll') }}">Delete </button> 
                                </div>
                              <div class="btn-group pull-left" data-toggle="buttons-radio"> 
                                <a href="{{route('festival.create',$flag)}}" style="color: white">
                                    <button type="button" class="btn">
                                        Add  {{ $flag }}</button></a>
                                        @if($flag != "video" )
                                    <a href="{{route('festival.create.multiimage',['id'=>'festival-list','flag'=>$flag])}}" style="color: white">
                                    <button type="button" class="btn">
                                       Multiple Add  {{ $flag }}</button></a>
                                       @endif
                                    </div>
                            </div>                           
                        </div>
                    </div>
                    <!--/.content-->
                </div>
                  </h4>
                @if (count($festival) >= 1)             
                <div class="btn-box-row row-fluid" style=" width: 105% !important;">
              @foreach($festival as $key=>$stickerarray)                       

                            <a href="#" class="btn-box big span4" style="margin-left: 0px; margin-right: 10px;">
                                @if($flag == 'sticker')
                                <img src="{{ url('storage/festival/sticker/'.$stickerarray->image) }}" style="width: 150px;height: 100px;"> @elseif($flag == 'photo')
                                  <img src="{{ url('storage/festival/'.$stickerarray->image) }}" style="width: 150px;height: 100px;">
                                  @else
                                   <video width="100" height="100" controls>
                                      <source src="{{ url('storage/festival/video/'.$stickerarray->video_status) }}" type="video/mp4">
                                        </video>
                                @endif
                                
                               <p class="text-muted">
                                <b style="font-size:17px;">{{ strlen($stickerarray->name) > 20 ? substr($stickerarray->name,0,20)."..." : $stickerarray->name  }}</b>
                                <input type="checkbox"  class="sub_chk" data-id="{{$stickerarray->id}}" style="width: 15px;ight: 15px;">
                                 <img src="{{ url('icon/delete-24px.svg') }}"  style="cursor: pointer;" onclick="deletesticker('{{$stickerarray->id}}')"></p>
                            </a> 
                 @endforeach
                 </div> 
                @endif                                            
            </div>                          
        </div>
</div>


<script src="{{ asset('mobile_app_css/js/core/jquery.min.js') }}"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

         <script src="{{ asset('scripts/jquery-1.9.1.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  var modelname = $('#sticker_model').val();
  var upload_folder = $('#upload_folder').val();
  
  function deletesticker(id) { 
      swal({
              title: "Are you sure?",
              text: "You will not be able to recover this data file!",
              type: "error",
              showCancelButton: true,
              dangerMode: true,
              cancelButtonClass: '#DD6B55',
              confirmButtonColor: '#dc3545',
              confirmButtonText: 'Delete!',
          }).then(function(isconform) { 
          if (isconform.value) {
            $.ajax({
                       url: "{{route('sticker.delete')}}",
                       type: 'post',
                        data: {
                          '_token':'{{ csrf_token() }}',                         
                          'id': id,                          
                      }, success: function(data) {
                           location.reload(true);
                          },
                  });
          }    
          });
              } 
  </script> 
 <script src="{{ asset('scripts/jquery-1.9.1.min.js') }}" type="text/javascript"></script>
   <script type="text/javascript">
    $(document).ready(function () {       
        $('#master').on('click', function(e) {
         if($(this).is(':checked',true))  
         {
            $(".sub_chk").prop('checked', true);  
         } else {  
            $(".sub_chk").prop('checked',false);  
         }  
        }); 


                  $('.delete_all').on('click', function (event) {
                      event.preventDefault();                      
                       var allVals = [];  
                        $(".sub_chk:checked").each(function() {  
                            allVals.push($(this).attr('data-id'));
                        });

                       if(allVals.length <= 0)  
                          {                              
                          swal({      
                          title: "Please select sticker.",                          
                          type: "error",                                          
                      });                         
                          }  else {
                          swal({
                          title: "Are you sure?",
                          text: "You will not be able to recover this data file!",
                          type: "error",
                          showCancelButton: true,
                          dangerMode: true,
                          cancelButtonClass: '#DD6B55',
                          confirmButtonColor: '#dc3545',
                          confirmButtonText: 'Delete!',
                      }).then(function(isconform) {
                        if (isconform.value) {                         
                            var join_selected_values = allVals.join(",");                   
                              $.ajax({
                                   url: "{{route('sticker.multidelete')}}",
                                   type: 'DELETE',
                                   data: {
                                      '_token':'{{ csrf_token() }}',
                                      'id': join_selected_values,                                           
                                     }, success: function(data) {
                                       location.reload(true);
                                      },
                              });
                            $.each(allVals, function( index, value ) {
                                $('table tr').filter("[data-row-id='" + value + "']").remove();
                            });        
                          }
                      });
                          }                      
                  }); 
      });
</script> 

  @endsection
