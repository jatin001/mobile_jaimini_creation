
@extends('layouts.master')
@section('content')
 <div class="span9">
        <div class="content">
            <div class="btn-controls">
 <div class="span9" style="float: right;">
                    <div class="content">
                        <div class="module"> 
                        <div class="module-head">
                                <h3>
                                   {{ $modeldetail['App_name'] }}</h3>
                            </div>                          
                            <div class="module-option clearfix">                                
                                <div class="btn-group pull-right" data-toggle="buttons-radio">
                                         Delete all record just check on  <input type="checkbox" id="master" style="width: 25px;height: 25px;margin-left: 10px;border-right-width: 10px;margin-right: 13px;">   <button style="background: red" class="btn btn-primary delete_all" data-url="{{ url('myproductsDeleteAll') }}">Delete </button> 
                                </div>
                              <div class="btn-group pull-left" data-toggle="buttons-radio"> <a href="{{route('stickerpack.create',$modeldetail['flag'])}}" style="color: white">
                                    <button type="button" class="btn">
                                        Add Pack </button></a></div>
                            </div>                           
                        </div>
                    </div>
                    <!--/.content-->
                </div>
                  </h4>
                 <input type="hidden" name="sticker_model" id="sticker_model" value="{{ $modeldetail['sticker_model'] }}">
                      <input type="hidden" name="upload_folder" id="upload_folder" value="{{ $modeldetail['upload_folder'] }}">
                @if (count($appstikerpack) >= 1)             
                <div class="btn-box-row row-fluid" style=" width: 105% !important;">
              @foreach($appstikerpack as $key=>$stickerarray)
                        <div class="btn-box-row row-fluid">

                            <a href="#" class="btn-box big span4">
                                <img src="{{ url('storage/sticker_pack/'.$stickerarray->image) }}" style="width: 150px;height: 100px;">
                                <b>{{ $stickerarray->name }}</b>
                               <p class="text-muted"> <input type="checkbox"  class="sub_chk" data-id="{{$stickerarray->id}}" style="width: 15px;ight: 15px;">
                                 <img src="{{ url('icon/delete-24px.svg') }}"  style="cursor: pointer;" onclick="deletesticker('{{$stickerarray->id}}')"></p>
                            </a> 
                 @endforeach
                 </div> 
                @endif                                            
            </div>                          
        </div>
</div>


<script src="{{ asset('mobile_app_css/js/core/jquery.min.js') }}"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

         <script src="{{ asset('scripts/jquery-1.9.1.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  var modelname = $('#sticker_model').val();
  var upload_folder = $('#upload_folder').val();
  
  function deletesticker(id) { 
      swal({
              title: "Are you sure?",
              text: "You will not be able to recover this data file!",
              type: "error",
              showCancelButton: true,
              dangerMode: true,
              cancelButtonClass: '#DD6B55',
              confirmButtonColor: '#dc3545',
              confirmButtonText: 'Delete!',
          }).then(function(isconform) { 
          if (isconform.value) {
            $.ajax({
                       url: "{{route('stickerpack.delete')}}",
                       type: 'post',
                        data: {
                          '_token':'{{ csrf_token() }}',                         
                          'id': id, 
                          'model':modelname,
                          'upload_folder':upload_folder,
                           'App':'App', 
                      }, success: function(data) {
                           location.reload(true);
                          },
                  });
          }    
          });
              } 
  </script> 
 <script src="{{ asset('scripts/jquery-1.9.1.min.js') }}" type="text/javascript"></script>
   <script type="text/javascript">
    $(document).ready(function () {       
        $('#master').on('click', function(e) {
         if($(this).is(':checked',true))  
         {
            $(".sub_chk").prop('checked', true);  
         } else {  
            $(".sub_chk").prop('checked',false);  
         }  
        }); 


                  $('.delete_all').on('click', function (event) {
                      event.preventDefault();                      
                       var allVals = [];  
                        $(".sub_chk:checked").each(function() {  
                            allVals.push($(this).attr('data-id'));
                        });

                       if(allVals.length <= 0)  
                          {                              
                          swal({      
                          title: "Please select sticker.",                          
                          type: "error",                                          
                      });                         
                          }  else {
                          swal({
                          title: "Are you sure?",
                          text: "You will not be able to recover this data file!",
                          type: "error",
                          showCancelButton: true,
                          dangerMode: true,
                          cancelButtonClass: '#DD6B55',
                          confirmButtonColor: '#dc3545',
                          confirmButtonText: 'Delete!',
                      }).then(function(isconform) {
                        if (isconform.value) {                         
                            var join_selected_values = allVals.join(",");                   
                              $.ajax({
                                   url: "{{route('stickerpack.multidelete')}}",
                                   type: 'DELETE',
                                   data: {
                                      '_token':'{{ csrf_token() }}',
                                      'id': join_selected_values, 
                                      'model' : modelname, 
                                       'upload_folder':upload_folder,  
                                       'App':'App',                                  
                                     }, success: function(data) {
                                       location.reload(true);
                                      },
                              });
                            $.each(allVals, function( index, value ) {
                                $('table tr').filter("[data-row-id='" + value + "']").remove();
                            });        
                          }
                      });
                          }                      
                  }); 
      });
</script> 

  @endsection
