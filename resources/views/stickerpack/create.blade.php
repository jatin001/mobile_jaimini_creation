
@extends('layouts.master')
@section('content')
<div class="span9">
          <div class="content">

            <div class="module">
              <div class="module-head">
                <h3>Forms</h3>
              </div>
              <div class="module-body">
                  <br>
                  <form class="form-horizontal row-fluid" method="POST" action="{{route('stickerpack.store')}}" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="sticker_model" value="{{ $modeldetail['sticker_model'] }}">
                          <input type="hidden" name="flag" value="{{ $modeldetail['flag'] }}">            
                         <input type="hidden" name="redirect_url" value="{{ $modeldetail['redirect_url'] }}">

                    <div class="control-group">
                      <label class="control-label" for="basicinput">Basic Input</label>
                      <div class="controls">
                         <input type="text" id="basicinput"  name="name" placeholder="Type something here..." class="span8">
                          @if($errors->has('name'))
                           <span class="error">
                            {{ $errors->first('name') }}
                                  </span>
                          @endif
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label" for="basicinput">Pack Files</label>
                      <div class="controls">
                        <input type="file" id="basicinput" accept="image/png" placeholder="Type something here..." class="span8" name="image">
                        @if($errors->has('image'))
                           <span class="error">
                            {{ $errors->first('image') }}
                                  </span>
                          @endif
                      </div>
                    </div>

                    <div class="control-group">
                      <div class="controls">
                        <button type="submit" class="btn">Submit Form</button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>

            
            
          </div><!--/.content-->
        </div>
         @endsection
<?php /*@extends('layouts.master')
@section('content') 

    <div class="main-panel">
      <!-- Navbar -->
       <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;">{{ $modeldetail['App_name']}}</a>
          </div>           
           <div class="collapse navbar-collapse justify-content-end">            
            <ul class="navbar-nav">
              <li class="nav-item">
                  <a href="{{ url()->previous() }}" style="color: white">
                 <button type="submit" class="btn btn-primary pull-right">                 
                 Back </button></a>                
              </li>
            </ul>
          </div>
          <!-- @include('layouts.sub_navigation') -->
      </nav> 
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Add Pack</h4>                
                </div>
                <div class="card-body">                 
                     <form name="form_influencer" method="POST" action="{{route('stickerpack.store')}}" enctype="multipart/form-data">
                               @csrf       
                   <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Pack Name</label>
                          <input type="hidden" name="sticker_model" value="{{ $modeldetail['sticker_model'] }}">
                          <input type="hidden" name="flag" value="{{ $modeldetail['flag'] }}">            
                         <input type="hidden" name="redirect_url" value="{{ $modeldetail['redirect_url'] }}">
                         
                          <input type="text" class="form-control" name="name">
                            @if($errors->has('name'))
                           <span class="error">
                            {{ $errors->first('name') }}
                                  </span>
                          @endif
                        </div>
                      </div>                      
                    </div>                    
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating"> Pack Sticker </label>
                          <input type="file" accept="image/png" class="form-control" name="image">
                          @if($errors->has('image'))
                           <span class="error">
                            {{ $errors->first('image') }}
                                  </span>
                          @endif
                        </div>
                      </div>
                    </div> 
                    <button type="submit" class="btn btn-primary pull-right" onclick="formsubmit()">Save</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>           
          </div>
        </div>
      </div>
      @include('layouts.footer')      
    </div>       
  </div>
</div>
<script>
function formsubmit(){
    document.getElementById("loading").style.display = "block";
  }
var _CANVAS = document.querySelector("#video-canvas"),
  _CTX = _CANVAS.getContext("2d"),
  _VIDEO = document.querySelector("#main-video");

document.querySelector("#file-to-upload").addEventListener('change', function() {
   
  document.querySelector("#main-video source").setAttribute('src', URL.createObjectURL(document.querySelector("#file-to-upload").files[0]));
 
  _VIDEO.load();
  _VIDEO.style.display = 'inline'; 
 
});

document.querySelector("#main-video").addEventListener('timeupdate', function() {
  _VIDEO.currentTime = 5;
   _CTX.drawImage(_VIDEO, 0, 0, 300, 150);
  document.querySelector("#put-thumbnail").setAttribute('value', _CANVAS.toDataURL());   
});
</script>
  @endsection*/ ?>