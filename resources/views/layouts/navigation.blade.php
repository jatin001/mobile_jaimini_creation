<div class="span3" style="height:450px;">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="{{route('home')}}"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>                             
                            </ul>
                            <ul class="widget widget-menu unstyled">
                                <li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Festival  </a>
                                    <ul id="togglePages" class="collapse unstyled">
                                         <li><a href="{{route('stickerpack.index','festival-pack')}}"><i class="icon-inbox"></i>Festival Pack </a></li>
                                        <li><a href="{{route('festival.index',['id'=>'festival-list','name'=>'sticker'])}}"><i class="icon-inbox"></i>Festival Sticker </a></li>
                                        <li><a href="{{route('festival.index',['id'=>'festival-list','name'=>'photo'])}}"><i class="icon-inbox"></i>Festival Image </a></li>
                                        <li><a href="{{route('festival.index',['id'=>'festival-list','name'=>'video'])}}"><i class="icon-inbox"></i>Festival Video Status </a></li>
                                    </ul>
                                </li>
                                <li>

                                     @if(Auth::user())
                        <a  href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                           <i class="menu-icon icon-signout"></i>Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>    
                    @endif
                   </li>
                            </ul>
                        </div>
                        <!--/.sidebar-->
                    </div>