<!DOCTYPE html>
<html lang="en">
    @include('layouts.header')   
  <body class="">
 <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Admin </a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">                        
                        <ul class="nav pull-right">                          
                            <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ asset('images/user.png')}}" class="nav-avatar" />
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Your Profile</a></li>
                                    <li><a href="#">Edit Profile</a></li>
                                    <li><a href="#">Account Settings</a></li>
                                    <li class="divider"></li>
                                    <li>@if(Auth::user())
                                            <a  href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                              Logout
                                            </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>    
                                  @endif
                              </li>
                                </ul>
                            </li>
                        </ul>
                    </div>                  
                </div>
            </div>
        </div>
 <div class="wrapper">
            <div class="container" style="margin-left: 5px;">
                  <div class="row">                 
                      @include('layouts.navigation')
                        @yield('content')
                 </div>
          </div>
        </div>
           @include('layouts.style')               
    </body>
</html>