@extends('layouts.master')
@section('content')
 <div class="span9">
        <div class="content">
            <div class="btn-controls">
                <div class="btn-box-row row-fluid">
                    <a href="{{route('festival.index',['id'=>'festival-list','name'=>'sticker'])}}" class="btn-box big span4"><i class=" icon-random"></i><b>65%</b>
                        <p class="text-muted">
                            Growth</p>
                    </a><a href="#" class="btn-box big span4"><i class="icon-user"></i><b>15</b>
                        <p class="text-muted">
                            New Users</p>
                    </a>
                    <a href="#" class="btn-box big span4"><i class="icon-money"></i><b>15,152</b>
                        <p class="text-muted">
                            Profit</p>
                    </a>
                </div>                                             
            </div>                          
        </div>
</div>
@endsection