<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFestivalTble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('festival', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('pack_id')->nullable();
            $table->longText('thumbs')->nullable();
            $table->string('image')->nullable();
            $table->string('video_status')->nullable();
            $table->string('video_thumbs')->nullable();
            $table->longText('description')->nullable();
            $table->enum('status', ['0','1','2'])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('festival');
    }
}
