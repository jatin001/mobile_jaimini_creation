<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/festival-list/{id}/{name}','App\Http\Controllers\FestivalController@index')->name('festival.index');

Route::get('festival/{id}', 'App\Http\Controllers\FestivalController@create')->name('festival.create');
Route::post('/festival', 'App\Http\Controllers\FestivalController@store')->name('festival.store');
Route::get('festivalmultiple/{id}', 'App\Http\Controllers\FestivalController@multiimage')->name('festival.create.multiimage');
Route::post('festival/{create}/savemultiimage', 'App\Http\Controllers\FestivalController@multipalfilesave')->name('festival.store.savemultiimage');

Route::post('sticker/destroy', 'App\Http\Controllers\FestivalController@destroy')->name('sticker.delete');
Route::delete('sticker/multidestroy', 'App\Http\Controllers\FestivalController@multidestroy')->name('sticker.multidelete');


Route::get('/stickerpack/{pack}','App\Http\Controllers\StickerpackController@index')->name('stickerpack.index');
Route::get('/stickerpackcreate/{create}','App\Http\Controllers\StickerpackController@create')->name('stickerpack.create');
Route::post('/stickerpack-pack', 'App\Http\Controllers\StickerpackController@store')->name('stickerpack.store');

Route::post('stickerpack/destroy', 'App\Http\Controllers\StickerpackController@destroy')->name('stickerpack.delete');
Route::delete('stickerpack/multidestroy', 'App\Http\Controllers\StickerpackController@multidestroy')->name('stickerpack.multidelete');