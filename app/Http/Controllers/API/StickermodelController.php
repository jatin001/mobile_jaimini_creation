<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\Festival;
use Illuminate\Support\Facades\Validator;

class StickermodelController extends Controller
{
    
    public function geteidappsticker(Request $request)
    {
    	 $validator = Validator::make($request->all(), [
                    'status' => 'required'
        ]);

        if ($validator->fails()) {
             $response = [
                'message' => $validator->messages()->first(),
            ];
            return response()->json($response, 422);
        }

         $response = array(); 
            if($request->status == '0'){  
                return $this->getstickerApi('festivalrelation','festival');                      
                   }else if($request->status == "1"){
                            return $this->getphoto($request->status);  
                   }else if($request->status == "2"){
                             return $this->getVideo($request->status);  
                   }else{
                        return $response = [
                            'message' => "Invalid status",
                            "status" => false,
                        ]; 
                  }                   
            }

     public function getphoto($status){
   
        $festival = Festival::where('status','=',$status)->get();  
        $response = array();  
        $counter = 0;
        if(count($festival) > 0){
            foreach ($festival as $value) {
                $response[$counter]['name'] = (string) null;
                if(isset($value->name)){
                   $response[$counter]['name'] = $value->name;
                }
                $response[$counter]['sticker'] =  (string) null;
                $response[$counter]['thumbs'] =  url('/storage/festival/default/default.png');                
                if(isset($value->image)){
                        $response[$counter]['sticker'] =  url('/storage/festival/'.$value->image);
                 }
                 if(isset($value->thumbs)){
                        $response[$counter]['thumbs'] =  url('/storage/festival/thumbs/'.$value->thumbs);
                 }
                 $counter++;
            }
        }
        return $response = [
            'message' => "Festival Photo detail",
            "data" => $response,
        ]; 
    }

     public function getVideo($status){
      $Festival = Festival::where('status','=',$status)->get();         
        $response = array();  
        $counter = 0;
        if(count($Festival) > 0){
            foreach ($Festival as $value) {
                $response[$counter]['name'] = (string) null;
                if(isset($value->name)){
                   $response[$counter]['name'] = $value->name;
                }
                $response[$counter]['video'] =  (string) null;
                $response[$counter]['thumbs'] =  (string) null;
                      if(isset($value->video_status)){
                           $response[$counter]['video'] =  url('/storage/festival/video/'.$value->video_status);
                        }                      
                       if(isset($value->video_thumbs)){
                               $response[$counter]['thumbs'] =  url('/storage/festival/video/thumbs/'.$value->video_thumbs); 
                          }                 
                 $counter++;
            }
        }
        return $response = [
            'message' => " video detail",
            "data" => $response,
        ]; 
    }

     public function getstickerApi($relation,$folder){
   
           $onampack = \App\Models\Stickerpack::with([$relation])->get();  
             $countestatus = 0;
             $response = array();
             if(count($onampack) >= 1){             
               foreach ($onampack as $key => $value) {               
                     if(count($value->$relation) >= 4){
                       $response[$countestatus]['identifier'] = (string) $value->id;
                       $response[$countestatus]['name'] = $value->name;
                       $response[$countestatus]['publisher'] = "Heena  patel";
                       $response[$countestatus]['tray_image_file'] = url('/storage/sticker_pack/thumb/'.$value->thumbs);
                       $response[$countestatus]['publisher_email'] = "patelheena2191@gmail.com";
                       $response[$countestatus]['publisher_website'] = (string) null;
                       $response[$countestatus]['privacy_policy_website'] = (string) null;
                       $response[$countestatus]['license_agreement_website'] = (string) null;                       
                       $onamarray = array();
                       $onamcount = 0;
                       $response[$countestatus]['stickers'] = [(string) null];  
                       if(count($value->$relation)){
                        foreach ($value->$relation as $onam) {
                             $onamarray[$onamcount]['image_file'] = url('/storage/'.$folder.'/sticker/'.$onam->image); 
                             $onamarray[$onamcount]['emojis'] = ["ðŸ™"];                                                 
                             $onamcount++;
                        } 
                         $response[$countestatus]['stickers'] = $onamarray;  
                        }     
                         $countestatus++;
                   }                                   
              }                                 
         }
         $android_play_store_link = (string) null; 
                  return $response = [
                    "android_play_store_link" => $android_play_store_link,                
                    "sticker_packs" => $response,
                ];
    }
}
