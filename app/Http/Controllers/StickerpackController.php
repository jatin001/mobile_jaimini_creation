<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use ZipArchive;
use File;
use App\Models\Stickerpack;
use DB;

class StickerpackController extends Controller
{
    public function index(Request $Request,$action) {
 
      $modeldetail = $this->checkApp($action);
      $appstikerpack = array();
      if(isset($modeldetail['sticker_model'])){
            $appstikerpack = Stickerpack::where('model_name','=',$modeldetail['sticker_model'])->get(); 
      }  
         return view('stickerpack.index',compact('appstikerpack','modeldetail'));
    }

    public function create(Request $request, $action){
       	$modeldetail = $this->checkApp($action);     
        return view('stickerpack.create',compact('modeldetail'));        
    }


    public function checkApp($flag){
    $model = array(); 
    if($flag == "festival-pack" || $flag == "festival"){
            $model['App_name'] = "Festival Pack";
            $model['flag'] = "festival-pack";            
            $model['sticker_model'] = "Festival";
            $model['upload_folder'] = "festival"; 
            $model['redirect_url'] = "/stickerpack/festival-pack";   
      } 
       return $model;   
  }


  public function store(Request $request){
      $this->validate($request, [
            'sticker_model' => 'required',
             'image' => 'mimes:png|required|max:49',                              
        ]); 
         $obstickerpack = new Stickerpack();
         $obstickerpack->name = $request->name;
         $obstickerpack->status = '0';
        $filename = '';
        if (!empty($request->file('image'))) {
             $image = $request->file('image');
              $fileoriginalname = pathinfo($request->file('image')->getClientOriginalName(), PATHINFO_FILENAME);                
              $filename =  $fileoriginalname.'_'. time().'.' . $request->file('image')->getClientOriginalExtension();
            
             $path = public_path('storage/sticker_pack');
                if(!file_exists($path.'/'.$filename)) {
                        $obstickerpack->image = $filename;
                        $obstickerpack->model_name = $request->sticker_model;
                        $obstickerpack->thumbs = $filename;
                        $obstickerpack->save();                       
               }                            
              $image->move($path, $filename);
              copy($path.'/'.$filename, $path.'/thumb/'.$filename);
               $smallthumbnailpath = public_path('storage/sticker_pack/thumb/'.$filename);
               $this->createThumbnail($smallthumbnailpath, 100, 100);

              // $path = public_path('storage/onam/sticker/'.$request->name);
              //   if(!File::isDirectory($path)){     
              //       File::makeDirectory($path, 0777, true, true);
              //   }            
         }           
         // toastr()->success('Data has been saved successfully!','Success!');
          return redirect($request->redirect_url);
    }

public function createThumbnail($path, $width, $height)
{	
    $img = Image::make($path)->resize($width, $height, function ($constraint) {
        $constraint->aspectRatio();
    });
    $img->save($path);
}

 public function destroy(Request $request){
     
      $obStickerpack = new Stickerpack();
      $stickerdetail = $obStickerpack->find($request->id);     
           if(!empty($stickerdetail) && !empty($stickerdetail->image)) { 
                 $model = $request->App."\Models\'". $request->model;
                      $modelname = str_replace("'", '', $model);
                      $newmodel  = new $modelname();
                      $childmodel  = $newmodel::where('pack_id','=',$request->id)->get();
                         if(count($childmodel) > 0){
                              foreach ($childmodel as $value) {
                                if(!empty($value->image)){
                                   if(file_exists(public_path('storage/'.$request->upload_folder.'/sticker/'.$value->image))) {                                      
                                        unlink(public_path('storage/'.$request->upload_folder.'/sticker/'.$value->image));
                                   }                 
                                }
                                  $childmodel  = $newmodel::where('id','=',$value->id)->delete();         
                              }
                         }
                           if(file_exists(public_path('storage/sticker_pack/'.$stickerdetail->image))) {
                                 unlink(public_path('storage/sticker_pack/'.$stickerdetail->image));
                           } 
                           if(file_exists(public_path('storage/sticker_pack/thumb/'.$stickerdetail->thumbs))) {
                                 unlink(public_path('storage/sticker_pack/thumb/'.$stickerdetail->image));
                           }  
                           $stickerdetail->delete();                   
                                  
           }            
    }
   

     public function multidestroy(Request $request){
         $obid =  explode(',', $request->id);  
         if(count($obid) > 0){
         $error = 0;
         $obStickerpack = new Stickerpack();
         $obid =  explode(',', $request->id);  
         if(count($obid) > 0){
                 foreach ($obid as $id) {
                 // $obWhatappsticker = new $request->model();
                      $model = $request->App."\Models\'". $request->model;
                      $modelname = str_replace("'", '', $model);
                      $newmodel  = new $modelname();
                      $childmodel  = $newmodel::where('pack_id','=',$id)->get();
                         if(count($childmodel) > 0){
                              foreach ($childmodel as $value) {
                                if(!empty($value->image)){
                                   if(file_exists(public_path('storage/'.$request->upload_folder.'/sticker/'.$value->image))) {                                      
                                        unlink(public_path('storage/'.$request->upload_folder.'/sticker/'.$value->image));
                                   }                 
                                }
                                 $childmodel  = $newmodel::where('id','=',$value->id)->delete();       
                              }
                         }                        
                        $stickerdetail = $obStickerpack->find($id); 
                        if(!empty($stickerdetail->image)){                              
                           if(file_exists(public_path('storage/sticker_pack/'.$stickerdetail->image))) {
                                 unlink(public_path('storage/sticker_pack/'.$stickerdetail->image));
                           } 
                           if(file_exists(public_path('storage/sticker_pack/thumb/'.$stickerdetail->thumbs))) {
                                 unlink(public_path('storage/sticker_pack/thumb/'.$stickerdetail->image));
                           }  
                           $stickerdetail->delete();                   
                        }                                            
                   }
         }else{
             $error = 1;
            // toastr()->error('Something went to wrong!','error!');
         } 
        if($error == 0){
          // toastr()->success('Sticker has been delete successfully!','Success!');             
         }
    }    
}
    }
