<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stickerpack extends Model
{
    protected $table = 'stickerpack';
    protected $fillable = [
        'id','image', 'name','thumbs','model_name'
    ];

   
    public function festivalrelation(){
        return $this->hasMany('App\Models\Festival','pack_id');
    }

     public function dussehrarelation(){
        return $this->hasMany('App\Dussehra','pack_id');
    }

      public function diwalirelation()
    {
         return $this->hasMany('App\Diwali','pack_id');
    }

     public function newyearrelation(){
        return $this->hasMany('App\model\Newyear','pack_id');
    }
       public function chhathpujarelation()
    {
         return $this->hasMany('App\model\Chhathpuja','pack_id');
    }

     public function christmasrelation()
    {
         return $this->hasMany('App\model\Christmas','pack_id');
    }

     public function bhaiyadoojrelation()
    {
         return $this->hasMany('App\model\Bhaiyadooj','pack_id');
    }

    public function childrandayrelation(){
        return $this->hasMany('App\model\Childranday','pack_id');
    }
     public function dhanterasrelation()
    {
         return $this->hasMany('App\model\Dhanteras','pack_id');
    }

     public function karwachauthrelation()
    {
         return $this->hasMany('App\model\Karwachauth','pack_id');
    }
}


